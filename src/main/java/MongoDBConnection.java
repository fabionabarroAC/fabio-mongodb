import com.mongodb.BasicDBObject;
import com.mongodb.client.*;
import org.bson.Document;

public class MongoDBConnection {
    public static void main(String[] args) {

//      Conection with secure mode
        String dbURI = "mongodb://admin:admin@localhost";
        MongoClient mongoClient = MongoClients.create(dbURI);

//      Check dbs at mongodb and show it at the terminal
        MongoIterable<String> dbnames = mongoClient.listDatabaseNames();
        for (String dbname : dbnames) {
           System.out.println("\t + Database: " + dbname );
      }

        MongoDatabase db = mongoClient.getDatabase("Profile");
        MongoCollection <Document> person = db.getCollection("person");

//      insert data
        Document document = new Document ("name","fabio");
        person.insertOne(document);

//      find datas and show it at the terminal
        FindIterable<Document> resultInsert = person.find(new Document("name","fabio"));
        System.out.println("\t Insert name:");
        for (Document document1 : resultInsert) {
            System.out.println("\t" + document1.toJson());
        }

//      update data
        System.out.println("\t Update name:");
        BasicDBObject query = new BasicDBObject();
        query.put("name", "fabio");

        BasicDBObject newDocument = new BasicDBObject();
        newDocument.put("name", "FABIO");

        BasicDBObject updateObject = new BasicDBObject();
        updateObject.put("$set", newDocument);

        db.getCollection("person").updateOne(query, updateObject);

        FindIterable<Document> resultUpdate = person.find(new Document("name","FABIO"));
        for (Document document1 : resultUpdate) {
            System.out.println("\t" + document1.toJson());
        }

//      delete data
        BasicDBObject removeDocument = new BasicDBObject();
        removeDocument.put("name", "FABIO");
        person.deleteOne(removeDocument);
        mongoClient.close();
    }
}
